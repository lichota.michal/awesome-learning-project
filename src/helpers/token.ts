const getToken = () => localStorage.getItem('token');

const setToken = (token: string) => localStorage.setItem('token', `Bearer ${token}`);

const deleteToken = () => localStorage.removeItem('token');

export {getToken, setToken, deleteToken}