import React from 'react';
import './App.css';
import AppLayout from './views/Layout';
import Login from "./views/Login/Login";
import useAuthContext from "./hooks/useAuthContext";
import useErrorHandler from "./hooks/useErrorHandler";

function App() {
    const {state} = useAuthContext();
    useErrorHandler();

    return (
        <>
            {state.isAuthenticated
                ?
                <AppLayout/>
                :
                <Login/>
            }
        </>
    );
}

export default App;
