import {Col, Layout, Menu, Row} from 'antd';
import {Link} from 'react-router-dom';
import {useLocation} from 'react-router-dom'
import useAuthContext from "../../hooks/useAuthContext";

const {Header} = Layout;

const getModuleNameFromLocationPath = (locationPath: string): string => locationPath.replace(/^\/|\/$/g, '').split('/')[0];

const LayoutHeader = () => {
    const location = useLocation();
    const {dispatch} = useAuthContext();
    return (
        <Header>
            <Row justify={"space-between"}>
                <Col flex={"auto"}>
                    <Menu theme="dark" mode="horizontal"
                          selectedKeys={[getModuleNameFromLocationPath(location.pathname)]}>
                        <Menu.Item key='users'>
                            <Link to='/users'>
                                Users
                            </Link>
                        </Menu.Item>
                        <Menu.Item key='invoices'>
                            <Link to='/invoices'>
                                Invoices
                            </Link>
                        </Menu.Item>

                    </Menu>
                </Col>
                <Col>
                    <Menu theme="dark" mode="horizontal">
                        <Menu.Item key='users' onClick={() => dispatch({type: 'LOGOUT'})}>
                                Wyloguj
                        </Menu.Item>
                    </Menu>
                </Col>
            </Row>
        </Header>)
}

export default LayoutHeader
