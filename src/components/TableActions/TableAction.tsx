import {Button} from "antd";
import React from "react";
import './TableAction-styles.css';

const TableAction = () => {
    return (
        <div>
            <Button className={'button'} type="primary" shape="round">Edit</Button>
            <Button className={'button'} danger={true} type="primary" shape="round">Delete</Button>
        </div>
    )
}

export default TableAction;
