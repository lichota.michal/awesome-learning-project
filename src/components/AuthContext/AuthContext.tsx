import React, {createContext, useReducer} from 'react';
import {deleteToken, getToken, setToken} from '../../helpers/token';

type TState = {
    isAuthenticated: boolean;
}

type TAction = {
    type: 'LOGIN' | 'LOGOUT'
    payload?: string,
}

const initialState =
    {isAuthenticated: !!getToken()}

export const AuthContext = createContext<{state: TState, dispatch: React.Dispatch<TAction>}>({state: initialState, dispatch: () => null});

const reducer = (state: TState, action: TAction): TState  => {
    switch (action.type) {
        case 'LOGIN':
            if (!action.payload) {
                return state
            }
            setToken(action.payload);
            return {
                isAuthenticated: true,
            };
        case 'LOGOUT':
            deleteToken();
            return {
                isAuthenticated: false,
            };
        default:
            return state;
    }
};

const AuthContextProvider: React.FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <AuthContext.Provider
            value={{
                state,
                dispatch,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};

export default AuthContextProvider