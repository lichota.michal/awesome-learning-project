import { useParams } from "react-router-dom";

const UserForm = () => {
    const params = useParams();
    const { userId } = params;
    
    return (<div>Welcome from users, userId: {userId}</div>)
}

export default UserForm;