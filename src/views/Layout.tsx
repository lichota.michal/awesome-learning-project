import {Layout} from 'antd';
import {Route, Routes, Navigate} from 'react-router-dom';
import Invoices from './Invoices/Invoices';
import InvoicesForm from './Invoices/InvoicesForm';
import UserForm from './Users/UserForm';
import Users from './Users/Users';
import LayoutHeader from "../components/LayoutHeader/LayoutHeader";

const {Content, Footer} = Layout;

const AppLayout = () => {
    return (<Layout className="layout">
        <LayoutHeader/>
        <Content style={{padding: '50px'}}>
            {/* <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb> */}
            <Routes>
                <Route path="users" element={<Users/>}/>
                <Route path="users/:userId" element={<UserForm/>}/>
                <Route path="invoices" element={<Invoices/>}/>
                <Route path="invoices/:invoiceId" element={<InvoicesForm/>}/>
                <Route path="*" element={<Navigate to="/users"/>}/>
            </Routes>
        </Content>
        <Footer style={{textAlign: 'center'}}>Awesome footer 2022</Footer>
    </Layout>)
}

export default AppLayout
