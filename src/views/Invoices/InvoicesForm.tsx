import { useParams } from "react-router-dom";

const InvoicesForm = () => {
    const params = useParams();
    const { invoiceId } = params;
    
    return (<div>Welcome from invoices, invoiceId: {invoiceId}</div>)
}

export default InvoicesForm;