import {Button, Card, Col, Form, Input, Row} from "antd";
import './Login-styles.css';
import Labels from "../../helpers/labels";
import useApiCall from "../../hooks/useApiCall";
import useAuthContext from "../../hooks/useAuthContext";

const Login = () => {
    const {call, isCalling} = useApiCall({path: '/auth/local', method: 'POST', callOnInit: false})
    const {dispatch} = useAuthContext();

    const onFinish = async (values) => {
        const result = await call({data: values});
        if (result?.status === 200) {
            dispatch({type: 'LOGIN', payload: result?.data?.jwt});
        }
    }

    return (
        <div className={'login-form-container'}>
            <Row justify={'center'}>
                <Col xs={24} sm={16} md={12} lg={8} xxl={4}>
                    <Card title={'Logowanie'}>
                        <Form
                            name="basic"
                            onFinish={onFinish}
                            layout='vertical'
                        >
                            <Form.Item
                                label="Użytkownik"
                                name="identifier"
                                rules={[{required: true, message: Labels['requiredField']}]}
                            >
                                <Input/>
                            </Form.Item>
                            <Form.Item
                                label="Hasło"
                                name="password"
                                rules={[{required: true, message: Labels['requiredField']}]}
                            >
                                <Input.Password/>
                            </Form.Item>
                            <Form.Item>
                                <Button loading={isCalling} className={'login-button'} type="primary" htmlType="submit">
                                    Zaloguj
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default Login