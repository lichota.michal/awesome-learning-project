import {useCallback, useEffect, useState} from "react";
import useApiClient from "./useApiClient";

type THttpMethods = 'GET' | 'POST' | 'PUT' | 'DELETE'

export type TParams = {
    pagination?: {
        page: number,
        pageSize: number
    },
    sort?: string
}

type TCallProps = {
    data?: any
    path?: string
    method?: THttpMethods
    params?: TParams
}

interface TUseApiCallProps extends TCallProps {
    callOnInit?: boolean
}

const useApiCall = (hookProps?: TUseApiCallProps) => {
    const method = hookProps?.method || 'GET'
    const path = hookProps?.path || ''
    const callOnStart = hookProps?.hasOwnProperty('callOnInit') ? hookProps?.callOnInit : true
    const data = hookProps?.data
    const params = hookProps?.params

    const apiClient = useApiClient();
    const [isCalling, setIsCalling] = useState(false);
    const [apiData, setApiData] = useState<any>();

    const call = useCallback(async (props?: TCallProps) => {
        setIsCalling(true)

        const result = await apiClient.request({
            url: props?.path || path,
            method: props?.method || method,
            data: props?.data || data,
            params: props?.params || params
        })

        setIsCalling(false)
        if (result.data) {
            setApiData(result.data);
        }
        return result;
    }, [apiClient, data, method, params, path])

    useEffect(() => {
        if (method && path && callOnStart) {
            call();
        }
    }, [call, method, path, callOnStart, params])

    return {call, isCalling, apiData}
}

export default useApiCall;
