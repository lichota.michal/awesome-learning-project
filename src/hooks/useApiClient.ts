import axios from 'axios';
import {stringify} from 'qs';

const adminApiClient = axios.create({
    baseURL: 'https://awesome-learning2.herokuapp.com/api',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
    paramsSerializer: params => stringify(params, {arrayFormat: 'repeat'}),
});

adminApiClient.interceptors.request.use(config => {
    const token = localStorage.getItem('token');
    if (token) {
        config.headers!.Authorization = token;
    }
    return config;
});

const useApiClient = () => {
    return adminApiClient
}

export default useApiClient;
