import {useContext} from "react";
import {AuthContext} from "../components/AuthContext/AuthContext";

const useAuthContext = () => useContext(AuthContext);

export default useAuthContext;