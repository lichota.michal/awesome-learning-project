import {notification} from "antd";
import useAuthContext from "./useAuthContext";
import useApiClient from "./useApiClient";

const openNotificationError = (message: string) => {
    notification.error({
        message: 'Błąd',
        description: message,
        placement: 'top'
    });
};

export const useErrorHandler = () => {
    const {dispatch} = useAuthContext();
    const adminApiClient = useApiClient();
    adminApiClient.interceptors.response.use(
        response => {
            return response;
        },
        error => {
            if (!error.response) {
                openNotificationError('Serwer nie odpowiada');
            } else {
                if ([401, 403].includes(error.response?.status)) {
                    dispatch({type: 'LOGOUT'});
                }
                openNotificationError(
                    error.response?.data?.error?.details?.error?.map(e => e.message).join(', ')
                    || error.response?.data?.error?.message
                    || error.response?.status
                );
            }
            return error
        },
    );
}

export default useErrorHandler;
