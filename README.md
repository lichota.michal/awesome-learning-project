# Awesome Learning Project

## Prerequisites

Git and node.js installed

## How to launch project

1. Clone repository `git clone https://gitlab.com/lichota.michal/awesome-learning-project.git`
2. `cd awesome-learning-project`
3. Run `npm install` to install depedencies
4. Run `npm run start` to start application (development)
